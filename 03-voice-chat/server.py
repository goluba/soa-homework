#!/usr/bin/python3

import socket
import select
import threading
from protocol import DataType, Protocol

PORT = 80

from threading import Lock

class Server:
    def __init__(self):
            self.ip = socket.gethostbyname('0.0.0.0')
            while 1:
                try:
                    self.port = PORT

                    self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                    self.s.bind((self.ip, self.port))

                    break
                except:
                    print("Couldn't bind to that port")

            self.connections = []
            self.mutex = Lock()
            self.accept_connections()

    def accept_connections(self):
        self.s.listen(100)

        print('Running on IP: '+self.ip)
        print('Running on port: '+str(self.port))
        
        while True:
            c, addr = self.s.accept()

            with self.mutex:
                self.connections.append((c, ""))

            threading.Thread(target=self.handle_client,args=(c,addr,)).start()
        
    def broadcast(self, sock, msg: Protocol, room: str):
        for client, cl_room in self.connections:
            if client != self.s and client != sock and cl_room == room:
                try:
                    client.send(msg.out())
                except:
                    pass

    def handle_client(self,c,addr):
        name = ""
        room = ""
        while 1:
            try:
                data = c.recv(1024)
                if not data:
                    continue
                msg = Protocol(datapacket=data)
                if msg.DataType == DataType.Handshake:
                    name, room = msg.data.decode(encoding='UTF-8').split()
                    text = "Client " + name + " connected to room #" + room + ";"
                    with self.mutex:
                        for i in range(len(self.connections)):
                            if self.connections[i][0] == c:
                                self.connections[i] = (c, room)
                                break
                    print(text)
                    self.broadcast(c, Protocol(dataType=DataType.Handshake, data=text.encode(encoding='UTF-8')), room)
                if msg.DataType == DataType.Disconnect:
                    text = "Client " + name + " disconnected;"
                    print(text)
                    self.connections.remove((c, room))
                    self.broadcast(c, Protocol(dataType=DataType.Handshake, data=text.encode(encoding='UTF-8')), room)
                self.broadcast(c, Protocol(dataType=DataType.ClientData, data=data), room)
            except socket.error:
                c.close()

server = Server()
