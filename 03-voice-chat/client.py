#!/usr/bin/python3

import socket
import threading
import pyaudio
from protocol import DataType, Protocol
import sys
import time

class Client:
    def __init__(self):
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        
        while 1:
            try:
                self.target_ip = input('Enter IP address of server --> ')
                self.target_port = int(input('Enter target port of server --> '))
                self.client_name = input('Enter your name --> ')
                self.client_room = input('Enter room id --> ')

                self.s.connect((self.target_ip, self.target_port))

                break
            except:
                print("Couldn't connect to server")

        chunk_size = 1024 # 512
        audio_format = pyaudio.paInt16
        channels = 1
        rate = 20000

        # initialise microphone recording
        self.p = pyaudio.PyAudio()
        self.playing_stream = self.p.open(format=audio_format, channels=channels, rate=rate, output=True, frames_per_buffer=chunk_size)
        self.recording_stream = self.p.open(format=audio_format, channels=channels, rate=rate, input=True, frames_per_buffer=chunk_size)
        
        print("Connected to Server")
        handshake = self.client_name + ' ' + self.client_room
        msg = Protocol(dataType=DataType.Handshake, data=handshake.encode(encoding='UTF-8'))
        self.s.sendall(msg.out())
        # start threads
        receive_thread = threading.Thread(target=self.receive_server_data).start()
        self.send_data_to_server()

    def receive_server_data(self):
        while True:
            try:
                data = self.s.recv(1024)
                msg = Protocol(datapacket=data)
                if msg.DataType == DataType.Handshake:
                    print(msg.data.decode(encoding='UTF-8'))
                else:
                    self.playing_stream.write(data)
            except:
                pass


    def send_data_to_server(self):
        while True:
            try:
                data = self.recording_stream.read(1024)
                self.s.sendall(Protocol(dataType=DataType.ClientData, data=data).out())
            except KeyboardInterrupt:
                print("Ctrl+C pressed...")
                self.s.sendall(Protocol(dataType=DataType.Disconnect, data=b'').out())
                self.s.close()
                time.sleep(2)
                sys.exit(1)
            except:
                pass

client = Client()
