import requests
import time

from bs4 import BeautifulSoup

from urllib.parse import urlparse, unquote


def getUrlsFromTextWithNetloc(netloc: str, textHtml: str):
    soup = BeautifulSoup(textHtml, 'html.parser')
    links = soup.select('a')
    data = []
    for link in links:
        href = link.get('href')
        if href != None:
            if 'https://' in href:
                url = href
            else:
                url = 'https://' + netloc + href
            if urlparse(url).netloc == netloc:
                data.append(unquote(url))
    return set(data)


def getUrlsFromPage(url: str):
    data = ""
    for _ in range(5):  # retry
        r = requests.get(url)
        if r.status_code == 200:
            data = r.text
            break
        time.sleep(0.05)  # 50ms
    return getUrlsFromTextWithNetloc(urlparse(url).netloc, data)


def restorePath(prev, v):
    path = [unquote(v)]
    while prev[v] is not None:
        v = prev[v]
        path.append(unquote(v))
    return path


def getPath(urls: list[str]):
    if urlparse(urls[0]).netloc != urlparse(urls[1]).netloc:
        return None, "[Error] Unable to find the path due to different netloc"
    if urls[0] == urls[1]:
        return [urls[0]], "OK"

    visited: dict[str, int] = {}
    dist: dict[str, int] = {}
    prev: dict[str, str] = {}

    visited[urls[0]] = 1
    prev[urls[0]] = None
    dist[urls[0]] = 0

    queue = [urls[0]]

    for _ in range(25):  # max steps
        next = []
        for page in queue:
            links = getUrlsFromPage(page)
            for link in links:
                if link not in visited:
                    visited[link] = 1
                    dist[link] = dist[page] + 1
                    prev[link] = page
                    next.append(link)
                if link == urls[1]:
                    path = list(reversed(restorePath(prev, link)))
                    return path, "OK"
                print("Processed pages:", len(visited), end='\r')
            queue = next
    return None, "[Error] Unable to find the path"
