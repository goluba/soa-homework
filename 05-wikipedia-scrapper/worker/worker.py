from scrapper import getPath

import pika
import json

connection = pika.BlockingConnection(pika.ConnectionParameters('rabbitmq', heartbeat=1800))

channel = connection.channel()

channel.queue_declare(queue='rpc_queue')

def on_request(ch, method, props, body):
    urls = json.loads(body)

    print('Processing:', urls[0], ' -???-> ', urls[1])
    response = getPath(urls)

    ch.basic_publish(exchange='',
                     routing_key=props.reply_to,
                     properties=pika.BasicProperties(correlation_id=props.correlation_id),
                     body=json.dumps(response))
    ch.basic_ack(delivery_tag=method.delivery_tag)

channel.basic_qos(prefetch_count=1)
channel.basic_consume(queue='rpc_queue', on_message_callback=on_request)

print(" [worker] Awaiting RPC requests")
channel.start_consuming()