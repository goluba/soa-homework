import json
import pika
import uuid

rabbitmq_host = "localhost"
rabbitmq_port = "5672"

class WikipediaScrapperRpcClient(object):

    def __init__(self):
        self.connection = pika.BlockingConnection(
            pika.ConnectionParameters(host=rabbitmq_host, port=rabbitmq_port))

        self.channel = self.connection.channel()

        result = self.channel.queue_declare(queue='', exclusive=True)
        self.callback_queue = result.method.queue

        self.channel.basic_consume(
            queue=self.callback_queue,
            on_message_callback=self.on_response,
            auto_ack=True)

    def on_response(self, ch, method, props, body):
        if self.corr_id == props.correlation_id:
            self.response = body

    def call(self, urls):
        self.response = None
        self.corr_id = str(uuid.uuid4())
        self.channel.basic_publish(
            exchange='',
            routing_key='rpc_queue',
            properties=pika.BasicProperties(
                reply_to=self.callback_queue,
                correlation_id=self.corr_id,
            ),
            body=json.dumps(urls))

        while self.response is None:
            self.connection.process_data_events()

        return json.loads(self.response)

rabbitmq_host, rabbitmq_port = input("RabbitMQ Server (host:port) ---> ").split(':')
client = WikipediaScrapperRpcClient()

while True:
    urls = ["", ""]
    urls[0] = input("URL #1 ---> ")
    urls[1] = input("URL #2 ---> ")

    print("Processing request ...", end='\r')
    path, error = client.call(urls)

    if path is None:
        print(error)
    else:
        print("Path found!")
        print("Path len:", len(path) - 1)
        print(" -> ".join(path))
