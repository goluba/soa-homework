# Wikipedia Scrapper с использованием очереди сообщений RabbitMQ
Выполенены все пункты задания, решение претендует на полный балл(`10`)

Для запуска сервера необходимо склонировать репозиторий и запустить
```(bash)
docker-compose build --pull
docker-compose up --scale worker=4
```

Докер-образ воркера находится по тегу `docker.io/goluba/wikipedia-scrapper`


Для подключения клиента необходимо установить зависимости и запустить `client.py` из папки `client`:
```(bash)
pip3 install -r requirements.txt
python3 client.py
```

Для подключения клиента необходимо использовать адрес сервера, где запущен `docker-compose` и порт `5672`

Пример работы приложения:
```
> ...
> pip3 install -r requirements.txt
> python3 client.py
RabbitMQ Server (host:port) ---> localhost:5672
URL #1 ---> https://ru.wikipedia.org/wiki/A4
URL #2 ---> https://ru.wikipedia.org/wiki/Wi-Fi
Path found!
Path len: 2
https://ru.wikipedia.org/wiki/A4 -> https://ru.wikipedia.org/wiki/Принтер -> https://ru.wikipedia.org/wiki/Wi-Fi
> ...
```