from flask import Flask
from flask import request, jsonify

from ariadne.constants import PLAYGROUND_HTML
from ariadne import load_schema_from_path, make_executable_schema, graphql_sync, ObjectType, fallback_resolvers

from api.queries import listGames_resolver, getGame_resolver, getScoreboard_resolver
from api.mutations import updateScoreboard_resolver, addComment_resolver

def create_app() -> Flask:
    """
    Create graphQL flask application
    """
    app = Flask(__name__)

    query = ObjectType("Query")
    query.set_field("listGames", listGames_resolver)
    query.set_field("getGame", getGame_resolver)
    query.set_field("getScoreboard", getScoreboard_resolver)

    mutation = ObjectType("Mutation")
    mutation.set_field("updateScoreboard", updateScoreboard_resolver)
    mutation.set_field("addComment", addComment_resolver)

    type_defs = load_schema_from_path("schema.graphql")
    schema = make_executable_schema(
        type_defs, query, mutation, fallback_resolvers
    )


    @app.route("/graphql", methods=["GET"])
    def graphql_playground():
        return PLAYGROUND_HTML, 200


    @app.route("/graphql", methods=["POST"])
    def graphql_server():
        data = request.get_json()

        success, result = graphql_sync(
            schema,
            data,
            context_value=request,
            debug=app.debug
        )

        status_code = 200 if success else 400
        return jsonify(result), status_code

    return app


app = create_app()

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
