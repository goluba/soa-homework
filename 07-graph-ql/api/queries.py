import api.state as state

def listGames_resolver(obj, info):
    try:
        games = []
        for id, game in state.games.items():
            games.append({'id': id, 'comments': game.comments})
        payload = {
            "success": True,
            "games": games
        }
    except Exception:
        payload = {
            "success": False,
        }
    return payload


def getGame_resolver(obj, info, id):
    if id in state.games:
        payload = {
            "success": True,
            "game": {'id': id, 'comments': state.games[id].comments}
        }
    else:
        payload = {
            "success": False,
        }
    return payload


def getScoreboard_resolver(obj, info, gameId):
    try:
        data = {}
        if gameId in state.scoreboard:
            for score in state.scoreboard[gameId]:
                data[score.userId] = score.score
        scores = [{'userId': userId, 'score': score} for userId, score in data.items()]
        payload = {
            "success": True,
            "scores": scores
        }
    except:
        payload = {
            "success": False,
        }
    return payload
