import api.state as state

def updateScoreboard_resolver(obj, info, gameId, userId, score):
    try:
        if gameId not in state.scoreboard:
            state.scoreboard[gameId] = []
        scoreObj = state.Score()
        scoreObj.userId = userId
        scoreObj.score = score
        state.scoreboard[gameId].append(scoreObj)
        payload = {
            "success": True,
            "scores": [{"userId": userId, "score": score}]
        }
    except ValueError:
        payload = {
            "success": False,
        }
    return payload

def addComment_resolver(obj, info, gameId, comment):
    try:
        if gameId not in state.games:
            state.games[gameId] = state.Game()
            state.games[gameId].comments = []
        state.games[gameId].comments.append(comment)
        payload = {
            "success": True,
            "game": [{"id": gameId, "comments": [comment]}]
        }
    except ValueError:
        payload = {
            "success": False,
        }

    return payload

