import typing

class Game:
    comments: list[str]
        
class Score:
    userId: str
    score: int

games: typing.Dict[str, Game] = {}
scoreboard: typing.Dict[str, list[Score]] = {}
