# Базовое приложение на основе GraphQL
Выполенены все пункты задания, решение претендует на `10` баллов

Докер образ с сервером можно найти по тегу `docker.io/goluba/graph-ql-state-server`

Для запуска сервера можно использовать команду `docker run -t -p 1234:5000 goluba/graph-ql-state-server`, где вместо `1234` можно указать желаемый порт.

Для подключения к серверу в качестве клиента необходимо открыть `http://localhost:<port>/graphql`, на этой странице можно выполнять graphQL запросы(надо подставить `<port>` с предыдущего шага)

Примеры запросов:
1.  ```graphql  
    mutation UpdateComment {
        addComment(gameId: "1", comment: "52") {
            success
        }
    }
    ```
2.  ```graphql
    mutation UpdateScoreboard {
        updateScoreboard(gameId: "1", userId: "1", score: 5) {
            success
        }
    }
    ```
3.  ```graphql
    query TestListGames {
        listGames {
            success
            games {
                id
                comments
            }
        }
    }
    ```
4.  ```graphql
    query TestGetGame {
        getGame(id: "1") {
            success
            game {
                id
                comments
            }
        }
    }
    ```
5.  ```graphql
    query ScoreboardTest {
        getScoreboard(gameId: "1") {
            success
            scores {
                userId
                score
            }
        }
    }
    ```

