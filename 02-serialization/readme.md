# Исследование методов сериализации данных
Докер образ с бенчмарком можно найти по тегу `docker.io/goluba/python-serialization-benchmark`

Были исследованы несколько библиотек для сериализации данный на языке Python:
1. pickle
2. simplejson
3. dicttoxml
4. protobuf
5. yaml
6. avro
7. msgpack
