"""
Dependencies:
 
    pip install tabulate simplejson numpy xmltodict dicttoxml numpy protobuf pyyaml fastavro msgpack
 
"""

from timeit import timeit 
from tabulate import tabulate
import sys 

message = '''d = {
    'PackageID' : 1539,
    'PersonID' : 33,
    'Name' : """MEGA_GAMER_2222""",
    'Inventory': dict((str(i),i) for i in iter(range(100))),  
    'CurrentLocation': """
		Pentos is a large port city, more populous than Astapor on Slaver Bay, 
		and may be one of the most populous of the Free Cities. 
		It lies on the bay of Pentos off the narrow sea, with the Flatlands 
		plains and Velvet Hills to the east.
		The city has many square brick towers, controlled by the spice traders. 
		Most of the roofing is done in tiles. There is a large red temple in 
		Pentos, along with the manse of Illyrio Mopatis and the Sunrise Gate 
		allows the traveler to exit the city to the east, 
		in the direction of the Rhoyne.
		""",
    'listInt': list(range(1000)),
    'listFloat': list(np.linspace(1, 100, num=500))
}'''

avro_schema = {
    'name': 'Data',
    'namespace': 'avro.benchmark',
    'type': 'record',
    'fields': [
        {'name': 'PackageID', 'type': 'int'},
        {'name': 'PersonID', 'type': 'int'},
        {'name': 'Name', 'type': 'string'},
        {'name': 'Inventory', 'type': {'type': 'map', 'values': 'int', 'default': {}}},
		{'name': 'CurrentLocation', 'type': 'string'},
        {'name': 'listInt', 'type': {'type': 'array', 'items': 'int', "default": []}},
        {'name': 'listFloat', 'type': {'type': 'array', 'items': 'float', "default": []}}
    ]
}

base_setup = 'import numpy as np ; '

setup_pickle    = base_setup + ('%s ; import pickle ; src = pickle.dumps(d, 2)' % message)
setup_json      = base_setup + ('%s ; import json; src = json.dumps(d)' % message)
setup_xml       = base_setup + ('%s ; import xmltodict, dicttoxml; src = dicttoxml.dicttoxml(d)' % message)
setup_protobuf  = base_setup + ('%s;' % message) + \
                                'from google.protobuf.json_format import ParseDict;' + \
                                'import test_pb2;' + \
                                'data = test_pb2.Data();' + \
                                'ParseDict(d, data);' + \
                                'src = data.SerializeToString();'
setup_yaml      = base_setup + ('%s ; import yaml; src = yaml.dump(d)' % message)
setup_avro      = base_setup + ('%s;' % message) + \
                                'from io import BytesIO;' + \
                                'from __main__ import avro_schema;' + \
                                'import fastavro;' + \
                                'src = BytesIO();' + \
                                'schema = fastavro.parse_schema(avro_schema);' + \
                                'fastavro.writer(src, schema, [d]);'
setup_msgpack   = base_setup + ('%s ; import msgpack ; src = msgpack.packb(d, use_bin_type=True)' % message)


tests = [
    # (title, setup, enc_test, dec_test) 
    # 'src' contains serialized data for dec_test
    ('pickle (native serialization)', setup_pickle, 'pickle.dumps(d, 2)', 'pickle.loads(src)'),
    ('json', setup_json, 'json.dumps(d)', 'json.loads(src)'),
    ('xml', setup_xml, 'dicttoxml.dicttoxml(d)', 'xmltodict.parse(src)'),
    ('protobuf', setup_protobuf, 'data = test_pb2.Data(); ParseDict(d, data); data.SerializeToString()', 'test_pb2.Data().ParseFromString(src)'),
    ('yaml', setup_yaml, 'yaml.dump(d)', 'yaml.load(src, yaml.Loader)'),
    ('avro', setup_avro, 'fastavro.writer(BytesIO(), schema, [d])', 'src.seek(0); reader = fastavro.reader(src); result_ = [data for data in reader]'),
    ('msgpack', setup_msgpack, 'msgpack.packb(d, use_bin_type=True)', 'msgpack.unpackb(src)'),
]
 
loops = 300
enc_table = []
dec_table = []
 
print("Running tests (%d loops each)" % loops)
 
for title, mod, enc, dec in tests:
    print(title)
 
    print("  [Encode]", enc) 
    result = timeit(enc, mod, number=loops)
    exec(mod) # generating src with serialized data
    enc_table.append([title, result, sys.getsizeof(src)])
 
    print("  [Decode]", dec) 
    result = timeit(dec, mod, number=loops)
    dec_table.append([title, result])
    sys.stdout.flush()
 
enc_table.sort(key=lambda x: x[1])
enc_table.insert(0, ['Package', 'Seconds', 'Size'])
 
dec_table.sort(key=lambda x: x[1])
dec_table.insert(0, ['Package', 'Seconds'])
 
print("\nEncoding Test (%d loops)" % loops)
print(tabulate(enc_table, headers="firstrow"))
 
print("\nDecoding Test (%d loops)" % loops)
print(tabulate(dec_table, headers="firstrow"))