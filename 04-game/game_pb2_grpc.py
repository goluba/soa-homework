# Generated by the gRPC Python protocol compiler plugin. DO NOT EDIT!
"""Client and server classes corresponding to protobuf-defined services."""
import grpc

import game_pb2 as game__pb2
from google.protobuf import empty_pb2 as google_dot_protobuf_dot_empty__pb2


class MafiaGameStub(object):
    """Missing associated documentation comment in .proto file."""

    def __init__(self, channel):
        """Constructor.

        Args:
            channel: A grpc.Channel.
        """
        self.SessionChat = channel.unary_stream(
                '/MafiaGame/SessionChat',
                request_serializer=google_dot_protobuf_dot_empty__pb2.Empty.SerializeToString,
                response_deserializer=game__pb2.Data.FromString,
                )
        self.SendMsg = channel.unary_unary(
                '/MafiaGame/SendMsg',
                request_serializer=game__pb2.Data.SerializeToString,
                response_deserializer=google_dot_protobuf_dot_empty__pb2.Empty.FromString,
                )


class MafiaGameServicer(object):
    """Missing associated documentation comment in .proto file."""

    def SessionChat(self, request, context):
        """Missing associated documentation comment in .proto file."""
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')

    def SendMsg(self, request, context):
        """Missing associated documentation comment in .proto file."""
        context.set_code(grpc.StatusCode.UNIMPLEMENTED)
        context.set_details('Method not implemented!')
        raise NotImplementedError('Method not implemented!')


def add_MafiaGameServicer_to_server(servicer, server):
    rpc_method_handlers = {
            'SessionChat': grpc.unary_stream_rpc_method_handler(
                    servicer.SessionChat,
                    request_deserializer=google_dot_protobuf_dot_empty__pb2.Empty.FromString,
                    response_serializer=game__pb2.Data.SerializeToString,
            ),
            'SendMsg': grpc.unary_unary_rpc_method_handler(
                    servicer.SendMsg,
                    request_deserializer=game__pb2.Data.FromString,
                    response_serializer=google_dot_protobuf_dot_empty__pb2.Empty.SerializeToString,
            ),
    }
    generic_handler = grpc.method_handlers_generic_handler(
            'MafiaGame', rpc_method_handlers)
    server.add_generic_rpc_handlers((generic_handler,))


 # This class is part of an EXPERIMENTAL API.
class MafiaGame(object):
    """Missing associated documentation comment in .proto file."""

    @staticmethod
    def SessionChat(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_stream(request, target, '/MafiaGame/SessionChat',
            google_dot_protobuf_dot_empty__pb2.Empty.SerializeToString,
            game__pb2.Data.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)

    @staticmethod
    def SendMsg(request,
            target,
            options=(),
            channel_credentials=None,
            call_credentials=None,
            insecure=False,
            compression=None,
            wait_for_ready=None,
            timeout=None,
            metadata=None):
        return grpc.experimental.unary_unary(request, target, '/MafiaGame/SendMsg',
            game__pb2.Data.SerializeToString,
            google_dot_protobuf_dot_empty__pb2.Empty.FromString,
            options, channel_credentials,
            insecure, call_credentials, compression, wait_for_ready, timeout, metadata)
