import grpc
import game_pb2
import game_pb2_grpc
from google.protobuf.empty_pb2 import Empty

from dataclasses import dataclass
from concurrent import futures
from enum import Enum
import random
import time

class PlayerRole(Enum):
    UNKNOWN = 0
    CITIZEN = 1
    MAFIA = 2
    COMMISSAR = 3
    LOSER = 4

class GameStatus(Enum):
    NOTSTARTED = 0
    SETUP = 1
    DAY = 2
    NIGHT = 3
    FINISHED = 4

class RoomState:
    def __init__(self):
        self._msgs = []
        self._players = {} # name -> playerRole
        self._game_status = GameStatus.NOTSTARTED
        self._ready = set()
        self._voted = {}
        self.mafia_is_known = None


class MafiaGameServicer(game_pb2_grpc.MafiaGameServicer):
    def __init__(self):
        self._rooms: dict[str, RoomState] = {} # roomId -> RoomState
        self._playerIds = {} # peer() -> name
        self._playerRooms = {} # peer() -> room

    # The stream which will be used to send new messages to clients
    def SessionChat(self, request_iterator, context):
        peer = context.peer()

        while peer not in self._playerRooms:
            time.sleep(0.1)
        room = self._playerRooms[peer]

        while peer not in self._playerIds:
            time.sleep(0.1)
        reciever = self._playerIds[peer]
        
        lastindex = 0
        while True:
            if lastindex < len(self._rooms[room]._msgs):
                lastindex += 1
                msg = self._rooms[room]._msgs[lastindex - 1][0]
                game_status = self._rooms[room]._msgs[lastindex - 1][1]
                if msg.name == "Server":
                    yield msg
                elif game_status in [GameStatus.DAY, GameStatus.FINISHED, GameStatus.NOTSTARTED]:
                    yield msg
                elif game_status == GameStatus.NIGHT:
                    if self._rooms[room]._players[msg.name] == self._rooms[room]._players[reciever]:
                        yield msg
                elif game_status == GameStatus.SETUP:
                    if msg.name == reciever:
                        yield msg

    # This method is called when a clients sends a message to the server.
    def SendMsg(self, request: game_pb2.Data, context):
        print("{}:[{}] {}".format(request.room, request.name, request.data))
        
        # Loser не может влиять на игру
        room = request.room
        
        peer = context.peer()
        if peer in self._playerIds and self._rooms[room]._players[self._playerIds[peer]] == PlayerRole.LOSER:
            return Empty()


        msg: str = request.data
        if msg.startswith('/'):
            command = msg[1:]
            if command.startswith("connect"):
                name = request.name
                if room not in self._rooms:
                    self._rooms[room] = RoomState()
                self._playerRooms[peer] = room
                if name not in self._rooms[room]._players:
                    self._playerIds[context.peer()] = name
                    self._rooms[room]._players[name] = PlayerRole.UNKNOWN
                    if self._rooms[room]._game_status != GameStatus.NOTSTARTED:
                        self._rooms[room]._players[name] = PlayerRole.LOSER
                data = "User " + name + " connected;"
                self._rooms[room]._msgs.append((game_pb2.Data(room=room, name='Server', data=data), self._rooms[room]._game_status))
            elif command.startswith("ready"):

                self._rooms[room]._ready.add(request.name)

                msg = "Users is ready: " + str(len(self._rooms[room]._ready))
                self._rooms[room]._msgs.append((game_pb2.Data(room=room, name=request.name, data=msg), GameStatus.SETUP))

                if self._rooms[room]._game_status == GameStatus.NOTSTARTED and len(self._rooms[room]._ready) == len(self._rooms[room]._players) and len(self._rooms[room]._ready) >= 4:
                    self.Setup(room)
                    self._rooms[room]._ready = set()
                elif self._rooms[room]._game_status == GameStatus.NIGHT and len(self._rooms[room]._ready) >= self.GetExpectedPending(room, GameStatus.NIGHT):
                    self.SetDay(room)
                    self._rooms[room]._ready = set()
                elif self._rooms[room]._game_status == GameStatus.DAY and len(self._rooms[room]._ready) >= self.GetExpectedPending(room, GameStatus.DAY):
                    self.Elections(room)
                    self.SetNight(room)
                    self._rooms[room]._ready = set()


            elif command.startswith("vote"):

                if self._rooms[room]._game_status == GameStatus.DAY:
                    name = command.split()[1]
                    if request.name not in self._rooms[room]._ready:
                        if name not in self._rooms[room]._voted:
                            self._rooms[room]._voted[name] = 0
                        self._rooms[room]._voted[name] += 1
                        self._rooms[room]._ready.add(request.name)
                elif self._rooms[room]._game_status == GameStatus.NIGHT:
                    victim = command.split()[1]
                    name = request.name
                    if self._rooms[room]._players[name] == PlayerRole.MAFIA:
                        self._rooms[room]._ready.add(name)
                        self._rooms[room]._players[victim] = PlayerRole.LOSER
                        msg = "Player " + victim + " was killed by mafia!"
                        self._rooms[room]._msgs.append((game_pb2.Data(room=room, name='Server', data=msg), self._rooms[room]._game_status))
                    elif self._rooms[room]._players[name] == PlayerRole.COMMISSAR:
                        self._rooms[room]._ready.add(name)
                        msg = "[System] Player " + victim + " is " + self._rooms[room]._players[victim].name + ";" 
                        if self._rooms[room]._players[victim] == PlayerRole.MAFIA:
                            self.mafia_is_known = victim
                        self._rooms[room]._msgs.append((game_pb2.Data(room=room, name=name, data=msg), self._rooms[room]._game_status))



            elif command.startswith("players"):

                data = "Players = [" + ';'.join(self._rooms[room]._players.keys()) + "];"
                self._rooms[room]._msgs.append((game_pb2.Data(room=room, name='Server', data=data), self._rooms[room]._game_status))

            elif command.startswith("who_is_ready"):

                data = "Ready = [" + ';'.join(list(self._rooms[room]._ready)) + "];"
                self._rooms[room]._msgs.append((game_pb2.Data(room=room, name=request.name, data=data), self._rooms[room]._game_status))

            elif command.startswith("publish_mafia"):

                if self._rooms[room]._players[request.name] == PlayerRole.COMMISSAR and self.mafia_is_known is not None:
                    msg = "Player " + victim + " is mafia!"
                    self._rooms[room]._msgs.append((game_pb2.Data(room=room, name='Server', data=msg), self._rooms[room]._game_status))

            else:
                self._rooms[room]._msgs.append((request, self._rooms[room]._game_status))
                self._rooms[room]._msgs.append((game_pb2.Data(room=room, name='Server', data="Invalid command"), self._rooms[room]._game_status))

        else:
            self._rooms[room]._msgs.append((request, self._rooms[room]._game_status))

        return Empty()

    def Setup(self, room):
        self._rooms[room]._msgs.append((game_pb2.Data(room=room, name="Server", data="The game is starting!"), self._rooms[room]._game_status))

        self._rooms[room]._game_status = GameStatus.SETUP
        players = list(self._rooms[room]._players.keys())
        for i in players:
            self._rooms[room]._players[i] = PlayerRole.CITIZEN
        mafia = random.choice(players)
        commissar = random.choice(players)
        while commissar == mafia:
            commissar = random.choice(players)
        self._rooms[room]._players[mafia] = PlayerRole.MAFIA
        self._rooms[room]._players[commissar] = PlayerRole.COMMISSAR

        for i in players:
            msg = "[System] You are " + self._rooms[room]._players[i].name + ";"
            self._rooms[room]._msgs.append((game_pb2.Data(room=room, name=i, data=msg), self._rooms[room]._game_status))

        self.SetDay(room)

    def Elections(self, room):
        name = None
        for i in self._rooms[room]._voted.keys():
            if name is None or self._rooms[room]._voted[name] < self._rooms[room]._voted[i]:
                name = i
        if name is not None and list(self._rooms[room]._voted.values()).count(self._rooms[room]._voted[name]) == 1:
            msg = "Player " + name + " is " + self._rooms[room]._players[name].name + "; Excluded from the game!" 
            self._rooms[room]._msgs.append((game_pb2.Data(room=room, name='Server', data=msg), self._rooms[room]._game_status))
            self._rooms[room]._players[name] = PlayerRole.LOSER
        else:
            self._rooms[room]._msgs.append((game_pb2.Data(room=room, name='Server', data="No one was elected"), self._rooms[room]._game_status))

    def CheckGameStatus(self, room):
        state = list(self._rooms[room]._players.values())
        if state.count(PlayerRole.MAFIA) == 0:
            self._rooms[room]._game_status = GameStatus.FINISHED
            self._rooms[room]._msgs.append((game_pb2.Data(room=room, name='Server', data="Citizens have won the game!"), self._rooms[room]._game_status))
            return
        if state.count(PlayerRole.CITIZEN) == state.count(PlayerRole.MAFIA):
            self._rooms[room]._game_status = GameStatus.FINISHED
            self._rooms[room]._msgs.append((game_pb2.Data(room=room, name='Server', data="Mafia have won the game!"), self._rooms[room]._game_status))
            return

    def SetDay(self, room):
        self.CheckGameStatus(room)
        if self._rooms[room]._game_status == GameStatus.FINISHED:
            return
        self._rooms[room]._msgs.append((game_pb2.Data(room=room, name='Server', data='The day is coming'), GameStatus.NIGHT))
        self._rooms[room]._msgs.append((game_pb2.Data(room=room, name='Server', data="Let's discuss"), GameStatus.NIGHT))
        self._rooms[room]._game_status = GameStatus.DAY

    def SetNight(self, room):
        self.CheckGameStatus(room)
        if self._rooms[room]._game_status == GameStatus.FINISHED:
            return
        self._rooms[room]._msgs.append((game_pb2.Data(room=room, name='Server', data='The night is coming'), GameStatus.DAY))
        self._rooms[room]._game_status = GameStatus.NIGHT
        for i in list(self._rooms[room]._players.keys()):
            if self._rooms[room]._players[i] == PlayerRole.MAFIA:
                self._rooms[room]._msgs.append((game_pb2.Data(room=room, name=i, data='[System] You are mafia! Make your choice'), GameStatus.NIGHT))
            if self._rooms[room]._players[i] == PlayerRole.COMMISSAR:
                self._rooms[room]._msgs.append((game_pb2.Data(room=room, name=i, data='[System] You are commissar! Make your choice'), GameStatus.NIGHT))

    def GetExpectedPending(self, room, mode):
        if mode == GameStatus.DAY:
            return len(self._rooms[room]._players) - list(self._rooms[room]._players.values()).count(PlayerRole.LOSER)
        elif mode == GameStatus.NIGHT:
            return list(self._rooms[room]._players.values()).count(PlayerRole.MAFIA) + list(self._rooms[room]._players.values()).count(PlayerRole.COMMISSAR)
        return 0


def serve():
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    game_pb2_grpc.add_MafiaGameServicer_to_server(MafiaGameServicer(), server)
    server.add_insecure_port('[::]:19137')
    server.start()
    print("Server is started!")
    server.wait_for_termination()

serve()

