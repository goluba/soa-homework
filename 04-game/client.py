import grpc
import game_pb2
import game_pb2_grpc
from google.protobuf.empty_pb2 import Empty

from enum import Enum
import threading
import time

address = 'localhost'
port = 19137

class Client:
    def __init__(self, name: str, room: str):
        self.name = name
        self.room = room
        channel = grpc.insecure_channel(address + ':' + str(port))
        self.stub = game_pb2_grpc.MafiaGameStub(channel)
        task = threading.Thread(target=self.__listen_for_messages)
        task.start()
        self.run()

    def __listen_for_messages(self):
        for msg in self.stub.SessionChat(Empty()):
            print("\r[{}] {}".format(msg.name, msg.data)) 

    def run(self):
        self.stub.SendMsg(game_pb2.Data(room=self.room, name=self.name, data='/connect'))
        while True:
            data = input()
            if len(data) > 0:
                self.stub.SendMsg(game_pb2.Data(room=self.room, name=self.name, data=data))

address =   input('Enter IP address of server --> ')
port =      input('Enter target port of server --> ')
name =      input('Enter your name --> ')
room =      input('Enter room id --> ')
Client(name, room)